<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\ModificationLog;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        return Post::all();
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required|string|max:255',
            'keywords' => 'required|string',
            'body' => 'required|string',
        ]);

        $post = Post::create($validated);
        ModificationLog::create([
            'post_id' => $post->id,
            'user_id' => auth()->id(),
            'action' => 'created'
        ]);
        return $post;
    }

/*
    public function store(Request $request)
    {
        $post = Post::create($request->all());
        ModificationLog::create([
            'post_id' => $post->id,
            'user_id' => auth()->id(),
            'action' => 'created'
        ]);
        return $post;
    }
*/
    public function update(Request $request, Post $post)
    {
        $post->update($request->all());
        ModificationLog::create([
            'post_id' => $post->id,
            'user_id' => auth()->id(),
            'action' => 'updated'
        ]);
        return $post;
    }

    public function destroy(Post $post)
    {
        ModificationLog::create([
            'post_id' => $post->id,
            'user_id' => auth()->id(),
            'action' => 'deleted'
        ]);
        $post->delete();
        return response()->noContent();
    }
}
